SELECT
	category_name,
	SUM(item_price)AS total_price
FROM
	item_category i1
RIGHT OUTER JOIN
	item i2
ON
	i1.category_id  = i2.category_id
GROUP BY
	category_name
ORDER BY
	total_price DESC;
